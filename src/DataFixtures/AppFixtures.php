<?php

namespace App\DataFixtures;

use App\Entity\SecurityGroup;
use App\Entity\SecurityRole;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /** @var UserPasswordEncoderInterface $encoder */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $role_emprunt_livre = new SecurityRole();
        $role_emprunt_livre->setLabel("ROLE_EMPRUNTE_LIVRE");
        $role_emprunt_livre->setIsActive(true);

        $manager->persist($role_emprunt_livre);

        $group = new SecurityGroup();
        $group->setLabel("GROUP_ADMINISTRATEUR");
        $group->addRole($role_emprunt_livre);
        $group->setIsActive(true);

        $manager->persist($group);

        $admin = new User();
        $admin->setUsername("Admin");
        $admin->setPassword($this->encoder->encodePassword($admin, "Admin"));
        $admin->setEmail("admin@bstorm.be");
        $admin->addGroup($group);
        $admin->setIsActive(true);

        $manager->persist($admin);

        $manager->flush();
    }
}
