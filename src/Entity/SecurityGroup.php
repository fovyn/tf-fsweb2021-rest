<?php

namespace App\Entity;

use App\Repository\SecurityGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SecurityGroupRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class SecurityGroup
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\ManyToMany(targetEntity=SecurityRole::class)
     */
    private $roles;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    use EntityTrait;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|SecurityRole[]
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    public function addRole(SecurityRole $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole(SecurityRole $role): self
    {
        $this->roles->removeElement($role);

        return $this;
    }
}
