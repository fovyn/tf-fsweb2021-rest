<?php


namespace App\Entity;


trait EntityTrait
{
    use TimestampTrait;
    use StateTrait;
}