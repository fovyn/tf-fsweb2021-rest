<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $email;

    /**
     * @ORM\ManyToMany(targetEntity=SecurityGroup::class)
     */
    private $groups;

    /**
     * @ORM\OneToMany(targetEntity=UserRoleTemp::class, mappedBy="owner")
     */
    private $userRoles;

    use EntityTrait;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
        $this->userRoles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|SecurityGroup[]
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(SecurityGroup $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
        }

        return $this;
    }

    public function removeGroup(SecurityGroup $group): self
    {
        $this->groups->removeElement($group);

        return $this;
    }

    /**
     * @return Collection|UserRoleTemp[]
     */
    public function getUserRoles(): Collection
    {
        return $this->userRoles;
    }

    public function addUserRole(UserRoleTemp $userRole): self
    {
        if (!$this->userRoles->contains($userRole)) {
            $this->userRoles[] = $userRole;
            $userRole->setOwner($this);
        }

        return $this;
    }

    public function removeUserRole(UserRoleTemp $userRole): self
    {
        if ($this->userRoles->removeElement($userRole)) {
            // set the owning side to null (unless already changed)
            if ($userRole->getOwner() === $this) {
                $userRole->setOwner(null);
            }
        }

        return $this;
    }

    public function getRoles() {
        $roles = [];

        //Ajoute tous les roles de tous les groupes auquels appartient l'utilisateur
        /** @var SecurityGroup $group */
        foreach ($this->groups as $group) {
            $roles[] = "ROLE_". $group->getLabel();
            foreach ($group->getRoles() as $role) {
                $roles[] = $role->getLabel();
            }
        }

        //Ajoute tous les roles possédés temporairement par l'utilisateur
        /** @var UserRoleTemp $userRole */
        foreach ($this->userRoles as $userRole) {
            if ($userRole->getEndAt() != null) {
                $roles[] = $userRole->getRole()->getLabel();
            }
        }

        dump($roles);
        return $roles;
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
