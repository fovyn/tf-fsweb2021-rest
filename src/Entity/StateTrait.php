<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Trait StateTrait
 * @package App\Entity
 * @ORM\HasLifecycleCallbacks()
 */
trait StateTrait
{
    /**
     * @var boolean $isActive
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private bool $isActive;

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @ORM\PrePersist()
     */
    public function createStateEvent() {
        $this->isActive = true;
    }
}