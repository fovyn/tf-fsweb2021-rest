<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Trait TimestampTrait
 * @package App\Entity
 * @ORM\HasLifecycleCallbacks()
 */
trait TimestampTrait
{
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private \DateTime $createdAt;
    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private \DateTime $updatedAt;

    /**
     * @ORM\PrePersist()
     */
    public function createEvent() {
        $this->createdAt = new \DateTime("now");
    }

    /**
     * @ORM\PreUpdate()
     */
    public function updateEvent() {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}