<?php

namespace App\Entity;

use App\Repository\LivreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Integer;

/**
 * @ORM\Entity(repositoryClass=LivreRepository::class)
 */
class Livre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="date")
     */
    private $dateAchat;

    /**
     * @ORM\OneToMany(targetEntity=LivreAuteurs::class, mappedBy="livre", orphanRemoval=true)
     */
    private $auteurs;

    public function __construct()
    {
        $this->auteurs = new ArrayCollection();
    }

    /**
     * @return Collection|LivreAuteurs[]
     */
    public function getAuteurs(): Collection
    {
        return $this->auteurs;
    }

    public function addAuteur(Auteur $auteur, int $droitAuteur): self
    {
        $livreAuteur = new LivreAuteurs();
        $livreAuteur->setAuteur($auteur);
        $livreAuteur->setLivre($this);
        $livreAuteur->setDroitAuteur($droitAuteur);

        if (!$this->auteurs->contains($livreAuteur)) {
            $this->auteurs[] = $auteur;
        }

        return $this;
    }

    public function removeAuteur(LivreAuteurs $auteur): self
    {
        if ($this->auteurs->removeElement($auteur)) {
            // set the owning side to null (unless already changed)
            if ($auteur->getLivre() === $this) {
                $auteur->setLivre(null);
            }
        }

        return $this;
    }
}
