<?php

namespace App\Repository;

use App\Entity\Auteur;
use App\Entity\Livre;
use App\Entity\LivreAuteurs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Livre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Livre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Livre[]    findAll()
 * @method Livre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LivreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Livre::class);
    }

    public function findByAuteurPseudoDQL(string $auteur) {
        $query = $this->getEntityManager()->createQuery("SELECT l FROM App\Entity\Livre l JOIN l.auteurs a WITH a.auteur = a");
//        $query = $this->_em->createQuery("SELECT l FROM App\Entity\Livre l INNER JOIN App\Entity\LivreAuteurs la WITH la.livre = l INNER JOIN App\Entity\Auteur a WITH la.auteur = a WHERE a.pseudo = :auteur");

        $query->setParameter("auteur", $auteur);

        return $query->getResult();
    }
    public function findByAuteurPseudo(string $auteur) {
        return $this->createQueryBuilder("l")
            ->innerJoin("l.auteurs", "a")
            ->andWhere("a.auteur.pseudo = :auteur")
            ->setParameter("auteur", $auteur)
            ->getQuery()->getResult();
    }
    // /**
    //  * @return Livre[] Returns an array of Livre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Livre
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
