<?php

namespace App\Repository;

use App\Entity\UserRoleTemp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserRoleTemp|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserRoleTemp|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserRoleTemp[]    findAll()
 * @method UserRoleTemp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRoleTempRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserRoleTemp::class);
    }

    // /**
    //  * @return UserRoleTemp[] Returns an array of UserRoleTemp objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserRoleTemp
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
