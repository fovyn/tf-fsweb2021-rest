<?php

namespace App\Repository;

use App\Entity\LivreAuteurs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LivreAuteurs|null find($id, $lockMode = null, $lockVersion = null)
 * @method LivreAuteurs|null findOneBy(array $criteria, array $orderBy = null)
 * @method LivreAuteurs[]    findAll()
 * @method LivreAuteurs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LivreAuteursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LivreAuteurs::class);
    }

    // /**
    //  * @return LivreAuteurs[] Returns an array of LivreAuteurs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LivreAuteurs
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
