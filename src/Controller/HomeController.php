<?php

namespace App\Controller;

use App\Repository\AuteurRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/home', name: 'home')]
    public function index(UserRepository $userRepository): Response
    {
        $users = $userRepository->findAll();

        return $this->render('home/index.html.twig', [
            "users" =>$users
        ]);
//        $auteurs = $repository->findByPseudoOrNom("flavian");
//        return $this->render('home/index.html.twig', [
//            'controller_name' => 'HomeController',
//            'users' => ["flavian", "evelyne", "jeremy", "jonas", "françois", "cédric"],
//            'auteurs' => $auteurs
//        ]);
    }

    /**
     * @Route("/admin", name="admin")
     * @IsGranted("ROLE_EMPRUNTE_LIVRE")
     * @return Response
     */
    public function secureAction(): Response {
        return $this->render("home/admin.html.twig", []);
    }
}
