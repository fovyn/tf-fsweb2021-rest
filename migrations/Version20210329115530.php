<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210329115530 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE auteur_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE livre_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE livre_auteurs_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE security_group_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE security_role_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE auteur (id INT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, pseudo VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE livre (id INT NOT NULL, titre VARCHAR(255) NOT NULL, date_achat DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE livre_auteurs (id INT NOT NULL, livre_id INT NOT NULL, auteur_id INT NOT NULL, droit_auteur SMALLINT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A0A4907E37D925CB ON livre_auteurs (livre_id)');
        $this->addSql('CREATE INDEX IDX_A0A4907E60BB6FE6 ON livre_auteurs (auteur_id)');
        $this->addSql('CREATE TABLE security_group (id INT NOT NULL, label VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, is_active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE security_group_security_role (security_group_id INT NOT NULL, security_role_id INT NOT NULL, PRIMARY KEY(security_group_id, security_role_id))');
        $this->addSql('CREATE INDEX IDX_42933CA9D3F5E95 ON security_group_security_role (security_group_id)');
        $this->addSql('CREATE INDEX IDX_42933CABBE829B1 ON security_group_security_role (security_role_id)');
        $this->addSql('CREATE TABLE security_role (id INT NOT NULL, label VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, is_active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, is_active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE user_security_group (user_id INT NOT NULL, security_group_id INT NOT NULL, PRIMARY KEY(user_id, security_group_id))');
        $this->addSql('CREATE INDEX IDX_EAF97836A76ED395 ON user_security_group (user_id)');
        $this->addSql('CREATE INDEX IDX_EAF978369D3F5E95 ON user_security_group (security_group_id)');
        $this->addSql('CREATE TABLE user_role_temp (owner_id INT NOT NULL, role_id INT NOT NULL, start_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, end_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(owner_id, role_id))');
        $this->addSql('CREATE INDEX IDX_B50C7ABA7E3C61F9 ON user_role_temp (owner_id)');
        $this->addSql('CREATE INDEX IDX_B50C7ABAD60322AC ON user_role_temp (role_id)');
        $this->addSql('ALTER TABLE livre_auteurs ADD CONSTRAINT FK_A0A4907E37D925CB FOREIGN KEY (livre_id) REFERENCES livre (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE livre_auteurs ADD CONSTRAINT FK_A0A4907E60BB6FE6 FOREIGN KEY (auteur_id) REFERENCES auteur (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE security_group_security_role ADD CONSTRAINT FK_42933CA9D3F5E95 FOREIGN KEY (security_group_id) REFERENCES security_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE security_group_security_role ADD CONSTRAINT FK_42933CABBE829B1 FOREIGN KEY (security_role_id) REFERENCES security_role (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_security_group ADD CONSTRAINT FK_EAF97836A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_security_group ADD CONSTRAINT FK_EAF978369D3F5E95 FOREIGN KEY (security_group_id) REFERENCES security_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_role_temp ADD CONSTRAINT FK_B50C7ABA7E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_role_temp ADD CONSTRAINT FK_B50C7ABAD60322AC FOREIGN KEY (role_id) REFERENCES security_role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE livre_auteurs DROP CONSTRAINT FK_A0A4907E60BB6FE6');
        $this->addSql('ALTER TABLE livre_auteurs DROP CONSTRAINT FK_A0A4907E37D925CB');
        $this->addSql('ALTER TABLE security_group_security_role DROP CONSTRAINT FK_42933CA9D3F5E95');
        $this->addSql('ALTER TABLE user_security_group DROP CONSTRAINT FK_EAF978369D3F5E95');
        $this->addSql('ALTER TABLE security_group_security_role DROP CONSTRAINT FK_42933CABBE829B1');
        $this->addSql('ALTER TABLE user_role_temp DROP CONSTRAINT FK_B50C7ABAD60322AC');
        $this->addSql('ALTER TABLE user_security_group DROP CONSTRAINT FK_EAF97836A76ED395');
        $this->addSql('ALTER TABLE user_role_temp DROP CONSTRAINT FK_B50C7ABA7E3C61F9');
        $this->addSql('DROP SEQUENCE auteur_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE livre_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE livre_auteurs_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE security_group_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE security_role_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP TABLE auteur');
        $this->addSql('DROP TABLE livre');
        $this->addSql('DROP TABLE livre_auteurs');
        $this->addSql('DROP TABLE security_group');
        $this->addSql('DROP TABLE security_group_security_role');
        $this->addSql('DROP TABLE security_role');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE user_security_group');
        $this->addSql('DROP TABLE user_role_temp');
    }
}
